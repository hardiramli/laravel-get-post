<?php namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\Registrar;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use App\User;
use Validator;
use Illuminate\Contracts\Auth\Registrar as RegistrarContract;
use Illuminate\Http\Request;


class AuthController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Registration & Login Controller
	|--------------------------------------------------------------------------
	|
	| This controller handles the registration of new users, as well as the
	| authentication of existing users. By default, this controller uses
	| a simple trait to add these behaviors. Why don't you explore it?
	|
	*/

	use AuthenticatesAndRegistersUsers;

	/**
	 * Create a new authentication controller instance.
	 *
	 * @param  \Illuminate\Contracts\Auth\Guard  $auth
	 * @param  \Illuminate\Contracts\Auth\Registrar  $registrar
	 * @return void
	 */
	public function __construct(Guard $auth, Registrar $registrar)
	{
		$this->auth = $auth;
		$this->registrar = $registrar;

		$this->middleware('guest', ['except' => 'getLogout']);
	}
	
	public function postRegister(Request $request){
		
		// create an account
		$email 	= $request->input('email');
		$name 	= $request->input('name');
		$password 	= $request->input('password');
		
		User::create([
		'name' => $name,
		'email' => $email,
		'password' => \Illuminate\Support\Facades\Hash::make($password)
			
		]);
		
		$code	= str_random(60);
		
		Mail::send('emails.auth.activate', 
			array('link' => URL::route('account-activate', $code), 'username' => $name), function($message) use ($userdata) {
			$message->to($userdata->email, $userdata->username)->subject('Activate your account');
		});	

		
	}
}
